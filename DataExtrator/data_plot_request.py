import numpy as np


class SIMONDataPlotRequest(object):
    __STEERING_WHEEL_MOVEMENT_ADDRESS = "SteeringWheelMovement"
    __PEDALS_MOVEMENT_ADDRESS = "PedalsMovement"
    __LANE_POSITION_DEVIATION_ADDRESS = "LanePositionDeviation"
    __LANE_ANGLE_DEVIATION_ADDRESS = "LaneAngleDeviation"
    __INSTANTANEOUS_SPEED_ADDRESS = "CurrentVelocity"
    __HEART_RATE_ADDRESS = "HeartRateUpdate"

    __COLLISION_DETECTED_ADDRESS = "CollisionDetected"
    __LANE_CHANGED_ADDRESS = "LaneChanged"

    def __init__(self, session, draw_mode):
        """
        :type session: list
        :type draw_mode: int
        """
        self.timestamps = [(entry[1] - session[0][1]).total_seconds() for entry in session]

        self.steering_wheel_movement_means, self.steering_wheel_movement_standard_deviation = \
            self.__get_measurement_data(session, self.__STEERING_WHEEL_MOVEMENT_ADDRESS, 2)
        self.throttle_movement, self.brake_movement = \
            self.__get_measurement_data(session, self.__PEDALS_MOVEMENT_ADDRESS, 2)
        self.lane_position_deviation_mean, self.lane_position_deviation_standard_deviation = \
            self.__get_measurement_data(session, self.__LANE_POSITION_DEVIATION_ADDRESS, 2)
        self.lane_angle_deviation_mean, self.lane_angle_deviation_standard_deviation = \
            self.__get_measurement_data(session, self.__LANE_ANGLE_DEVIATION_ADDRESS, 2)
        self.instantaneous_speed_mean, self.instantaneous_speed_standard_deviation = \
            self.__get_measurement_data(session, self.__INSTANTANEOUS_SPEED_ADDRESS, 2)
        self.heart_rate = self.__get_measurement_data(session, self.__HEART_RATE_ADDRESS, 1)
        self.heart_rate_mean = np.mean(self.heart_rate)  # type:float

        self.collision_detections = \
            self.__get_event_data(session, self.__COLLISION_DETECTED_ADDRESS, 1, session[0][1])

        self.lane_changes = self.__get_event_data(session, self.__LANE_CHANGED_ADDRESS, 0, session[0][1])

        self.draw_mode = draw_mode

    @property
    def start_timestamp(self):
        """
        :rtype: float
        """
        return self.timestamps[0]

    @property
    def stop_timestamp(self):
        """
        :rtype: float
        """
        return self.timestamps[-1]

    @staticmethod
    def __get_measurement_data(session, address, n_params_per_address):
        """
        :type session: list
        :type address: str
        :type n_params_per_address: int
        :rtype: (np.ndarray, np.ndarray)
        """
        params = []
        for idx in range(n_params_per_address):
            params.append([])

        last_timestamp = -1
        idx = 0
        for entry in session:
            if address in entry[0]:
                if last_timestamp != entry[1]:
                    idx = 0

                for curr_idx in range(n_params_per_address):
                    param = params[curr_idx]
                    if curr_idx == idx:
                        param.append(entry[2])
                    else:
                        param.append(0 if len(param) == 0 else param[-1])
                idx += 1
            else:
                for curr_idx in range(n_params_per_address):
                    param = params[curr_idx]
                    param.append(0 if len(param) == 0 else param[-1])

            last_timestamp = entry[1]

        return params[0] if n_params_per_address == 1 else params

    @staticmethod
    def __get_event_data(session, address, data_idx, start_timestamp):
        """
        :type session: list
        :type address: str
        :type data_idx: int
        :rtype: list
        """
        event_data = []
        last_timestamp = -1
        event_entry_idx = 0
        for entry in session:
            if address in entry[0]:
                if last_timestamp != entry[1]:
                    event_entry_idx = 0

                if event_entry_idx == data_idx:
                    event_data.append(((entry[1] - start_timestamp).total_seconds(), entry[2]))

                last_timestamp = entry[1]
                event_entry_idx += 1
        return event_data
