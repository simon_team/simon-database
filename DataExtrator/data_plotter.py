import matplotlib.pyplot as plt
import os
import numpy as np

from data_plot_request import SIMONDataPlotRequest

# region Draw Modes
CLEAN_PLOT = 0
PLOT_LEGEND = 1
COMBINE_EVENTS = 2
COMPLETE_PLOT = PLOT_LEGEND | COMBINE_EVENTS
BULK_MODE = 4

# endregion

# region Colors
ORANGE = (1, 0.5, 0)
DARK_ORANGE = (.5, 0.25, 0)


# endregion

class SIMONDataPlotter(object):
    __DEFAULT_TITLE_PREFIX = "SIMON Session"
    __BULK_PLOT_MARGINS_COMBINE_EVENTS = (.03, .04, .99, .97, .06, .22)
    __BULK_PLOT_MARGINS_SEPARATED_EVENTS = (.03, .04, .99, .97, .1, .23)
    __BULK_RENDER_MARGINS_COMBINE_EVENTS = (.0475, .05, .99, .97, .09, .3)
    __BULK_RENDER_MARGINS_SEPARATED_EVENTS = (.0475, .05, .99, .97, .15, .31)
    __RENDER_MARGINS = (.0475, .06, .99, .97, .06, .22)

    __STEERING_WHEEL_MOVEMENT_ADDRESS = "SteeringWheelMovement"
    __PEDALS_MOVEMENT_ADDRESS = "PedalsMovement"
    __LANE_POSITION_DEVIATION_ADDRESS = "LanePositionDeviation"
    __LANE_ANGLE_DEVIATION_ADDRESS = "LaneAngleDeviation"
    __INSTANTANEOUS_SPEED_ADDRESS = "CurrentVelocity"
    __HEART_RATE_ADDRESS = "HeartRateUpdate"

    __COLLISION_DETECTED_ADDRESS = "CollisionDetected"
    __LANE_CHANGED_ADDRESS = "LaneChanged"

    def __init__(self):
        self.__title_prefix = self.__DEFAULT_TITLE_PREFIX

    def set_title_prefix(self, title_prefix):
        """
        :type title_prefix: str
        """
        self.__title_prefix = title_prefix

    # region Plot Functions
    def plot_sessions(self, sessions, blocking_plot=False, draw_mode=COMPLETE_PLOT):
        """
        :type sessions: list
        :type blocking_plot: bool
        :type draw_mode: int
        """
        for idx in range(len(sessions)):
            title = self.__title_prefix + " " + str(idx + 1)
            self.plot_session(sessions[idx], title, blocking_plot, draw_mode)

    def plot_session(self, session, title=__DEFAULT_TITLE_PREFIX, blocking_plot=False, draw_mode=COMPLETE_PLOT):
        """
        :type session: list
        :type title: str
        :type blocking_plot: bool
        :type draw_mode: int
        """
        print "Plotting " + title + "...",

        session_request = SIMONDataPlotRequest(session, draw_mode)

        plt.figure(title, figsize=(16, 9))

        if self.__has_draw_mode(session_request.draw_mode, COMBINE_EVENTS):
            margins = self.__BULK_PLOT_MARGINS_COMBINE_EVENTS
        else:
            margins = self.__BULK_PLOT_MARGINS_SEPARATED_EVENTS
        self.__draw_bulk_plot(session_request, margins)

        plt.show(blocking_plot)
        print "Done!"

    # endregion

    # region Render Functions
    def render_sessions(self, sessions, save_directory, save_resolution=300, draw_mode=COMPLETE_PLOT):
        """
        :type sessions: list
        :type save_directory: str
        :type save_resolution: int
        :type draw_mode: int
        """
        for idx in range(len(sessions)):
            title = self.__title_prefix + " " + str(idx + 1)
            self.render_session(sessions[idx], save_directory, save_resolution, title, draw_mode)

    def render_session(self, session, save_directory, save_resolution=300,
                       title=__DEFAULT_TITLE_PREFIX, draw_mode=COMPLETE_PLOT):
        """
        :type session: list
        :type save_directory: str
        :type save_resolution: int
        :type title: str
        :type draw_mode: int
        """
        print "(" + str(session[0][1]) + ") Rendering " + title + " (" + self.__get_draw_mode_print_string(
            draw_mode) + ")...",

        session_request = SIMONDataPlotRequest(session, draw_mode)

        if self.__has_draw_mode(draw_mode, BULK_MODE):
            self.__bulk_render_session(session_request, save_directory, save_resolution, title)
        else:
            self.__individual_render_session(session_request, save_directory, save_resolution, title)

        print "Done!"

        self.__print_data_values(session_request)

    # TODO HACK
    def __print_data_values(self, session_request):
        print "   (SWM Mean) Min:", np.min(session_request.steering_wheel_movement_means), \
            "| Max:", np.max(session_request.steering_wheel_movement_means), \
            "| Mean:", np.mean(session_request.steering_wheel_movement_means)

        print "   (SWM Standard Deviation) Min:", np.min(session_request.steering_wheel_movement_standard_deviation), \
            "| Max:", np.max(session_request.steering_wheel_movement_standard_deviation), \
            "| Mean:", np.mean(session_request.steering_wheel_movement_standard_deviation)

        print "   (Throttle Movement) Min:", np.min(session_request.throttle_movement), \
            "| Max:", np.max(session_request.throttle_movement), \
            "| Mean:", np.mean(session_request.throttle_movement)

        print "   (Brake Movement) Min:", np.min(session_request.brake_movement), \
            "| Max:", np.max(session_request.brake_movement), \
            "| Mean:", np.mean(session_request.brake_movement)

        print "   (Lane Position Deviation Mean) Min:", np.min(session_request.lane_position_deviation_mean), \
            "| Max:", np.max(session_request.lane_position_deviation_mean), \
            "| Mean:", np.mean(session_request.lane_position_deviation_mean)

        print "   (Lane Position Deviation Standard Deviation) Min:", \
            np.min(session_request.lane_position_deviation_standard_deviation), \
            "| Max:", np.max(session_request.lane_position_deviation_standard_deviation), \
            "| Mean:", np.mean(session_request.lane_position_deviation_standard_deviation)

        print "   (Lane Angle Deviation Mean) Min:", np.min(session_request.lane_angle_deviation_mean), \
            "| Max:", np.max(session_request.lane_angle_deviation_mean), \
            "| Mean:", np.mean(session_request.lane_angle_deviation_mean)

        print "   (Lane Angle Deviation Standard Deviation) Min:", \
            np.min(session_request.lane_angle_deviation_standard_deviation), \
            "| Max:", np.max(session_request.lane_angle_deviation_standard_deviation), \
            "| Mean:", np.mean(session_request.lane_angle_deviation_standard_deviation)

        print "   (Speed Mean) Min:", np.min(session_request.instantaneous_speed_mean), \
            "| Max:", np.max(session_request.instantaneous_speed_mean), \
            "| Mean:", np.mean(session_request.instantaneous_speed_mean)

        print "   (Speed Standard Deviation) Min:", np.min(session_request.instantaneous_speed_standard_deviation), \
            "| Max:", np.max(session_request.instantaneous_speed_standard_deviation), \
            "| Mean:", np.mean(session_request.instantaneous_speed_standard_deviation)

        print "   (Heart Rate) Min:", np.min(session_request.heart_rate), \
            "| Max:", np.max(session_request.heart_rate), \
            "| Mean:", np.mean(session_request.heart_rate_mean)

        # TODO numero de colisoes e de mudancas de faixa
        print "   Number of Collisions", len(session_request.collision_detections)
        print "   Number of Lane Changes", len(session_request.lane_changes)

    def __individual_render_session(self, session, save_directory, save_resolution, title):
        """
        :type session: SIMONDataPlotRequest
        :type save_directory: str
        :type save_resolution: int
        :type title: str
        """
        self.__draw_and_save_measurement(self.__draw_steering_wheel_movement, session, title,
                                         save_directory, save_resolution)
        self.__draw_and_save_measurement(self.__draw_pedals_movement, session, title, save_directory, save_resolution)
        self.__draw_and_save_measurement(self.__draw_lane_position_deviation, session, title,
                                         save_directory, save_resolution)
        self.__draw_and_save_measurement(self.__draw_lane_angle_deviation, session, title,
                                         save_directory, save_resolution)
        self.__draw_and_save_measurement(self.__draw_instantaneous_speed, session, title,
                                         save_directory, save_resolution)
        self.__draw_and_save_measurement(self.__draw_heart_rate, session, title, save_directory, save_resolution)

        if not self.__has_draw_mode(session.draw_mode, COMBINE_EVENTS):
            self.__draw_and_save_measurement(self.__draw_events, session, title, save_directory, save_resolution)

    def __bulk_render_session(self, session, save_directory, save_resolution, title):
        """
        :type session: SIMONDataPlotRequest
        :type save_directory: str
        :type save_resolution: int
        :type title: str
        """
        figure = plt.figure(title, figsize=(16, 9))

        if self.__has_draw_mode(session.draw_mode, COMBINE_EVENTS):
            margins = self.__BULK_RENDER_MARGINS_COMBINE_EVENTS
        else:
            margins = self.__BULK_RENDER_MARGINS_SEPARATED_EVENTS
        self.__draw_bulk_plot(session, margins)

        figure.savefig(
            "{}/{}{}.png".format(save_directory, title, self.__get_draw_mode_file_extension(session.draw_mode)),
            dpi=save_resolution)

        plt.close(figure)

    def __draw_and_save_measurement(self, plot_function, session, session_title, save_directory,
                                    save_resolution, y_limits=None):
        """
        :type plot_function: Callable
        :type session: SIMONDataPlotRequest
        :type session_title: str
        :type save_directory: str
        :type save_resolution: int
        """
        figure = plt.figure(figsize=(16, 9))
        figure_title = self.__draw(session, plot_function, y_limits)

        figure.canvas.set_window_title(session_title + " - " + figure_title)
        plt.subplots_adjust(*self.__RENDER_MARGINS)

        save_directory = "{}/{}".format(save_directory, session_title)
        if not os.path.exists(save_directory):
            os.makedirs(save_directory)
        figure.savefig(
            "{}/{}{}.png".format(save_directory, figure_title,
                                 '.' + self.__get_draw_mode_file_extension(session.draw_mode)),
            dpi=save_resolution)

        plt.close(figure)

    # endregion

    # region Draw Methods
    @classmethod
    def __draw_bulk_plot(cls, session, margins):
        """
        :type session: SIMONDataPlotRequest
        :type margins: tuple
        """
        if cls.__has_draw_mode(session.draw_mode, COMBINE_EVENTS):
            cls.__draw_bulk_plot_events_combined(session)
        else:
            cls.__draw_bulk_plot_events_separated(session)
        plt.subplots_adjust(*margins)

    @classmethod
    def __draw_bulk_plot_events_combined(cls, session):
        """
        :type session: SIMONDataPlotRequest
        """
        plt.subplot(3, 2, 1)
        cls.__draw(session, cls.__draw_steering_wheel_movement)
        plt.subplot(3, 2, 2)
        cls.__draw(session, cls.__draw_pedals_movement)
        plt.subplot(3, 2, 3)
        cls.__draw(session, cls.__draw_lane_position_deviation)
        plt.subplot(3, 2, 4)
        cls.__draw(session, cls.__draw_lane_angle_deviation)
        plt.subplot(3, 2, 5)
        cls.__draw(session, cls.__draw_instantaneous_speed)
        plt.subplot(3, 2, 6)
        cls.__draw(session, cls.__draw_heart_rate)

    @classmethod
    def __draw_bulk_plot_events_separated(cls, session):
        """
        :type session: SIMONDataPlotRequest
        """
        plt.subplot(3, 3, 1)
        cls.__draw(session, cls.__draw_steering_wheel_movement)
        plt.subplot(3, 3, 2)
        cls.__draw(session, cls.__draw_pedals_movement)
        plt.subplot(3, 3, 3)
        cls.__draw(session, cls.__draw_lane_position_deviation)
        plt.subplot(3, 3, 4)
        cls.__draw(session, cls.__draw_lane_angle_deviation)
        plt.subplot(3, 3, 5)
        cls.__draw(session, cls.__draw_instantaneous_speed)
        plt.subplot(3, 3, 6)
        cls.__draw(session, cls.__draw_heart_rate)
        plt.subplot(3, 1, 3)
        cls.__draw(session, cls.__draw_events)

    @classmethod
    def __draw(cls, session, plot_function, y_limits=None):
        """
        :type session: SIMONDataPlotRequest
        :type plot_function: Callable
        :type y_limits: tuple
        :return: Title of the figure plotted
        :rtype: str
        """
        handles, y_label, plot_title, figure_title = plot_function(session)

        changed_to_left_lane_plot = None
        changed_to_right_lane_plot = None
        collisions_plot = None
        if (session.draw_mode & COMBINE_EVENTS) != 0:
            (changed_to_left_lane_plot, changed_to_right_lane_plot, collisions_plot), _, _, _ = \
                cls.__draw_events(session)
            # for lane_change in session.lane_changes:
            #     if "Left" in lane_change[1]:
            #         changed_to_left_lane_plot, = plt.plot((lane_change[0], lane_change[0]), plt.ylim(),
            #                                               color=DARK_ORANGE, label="Changed to Left Lane", zorder=-1)
            #     else:
            #         changed_to_right_lane_plot, = plt.plot((lane_change[0], lane_change[0]), plt.ylim(),
            #                                                color=ORANGE, label="Changed to Right Lane", zorder=-1)
            #
            # for collision in session.collision_detections:
            #     collisions_plot, = plt.plot((collision[0], collision[0]), plt.ylim(), 'r', label="Collisions",
            #                                 zorder=-1)

        plt.title(plot_title)

        plt.xlim(session.start_timestamp, session.stop_timestamp)
        plt.xlabel("Seconds")

        if y_limits is not None:
            plt.ylim(y_limits)
        if y_label is not None:
            plt.ylabel(y_label)

        if handles is not None:
            handles = [handle for handle in handles if handle is not None]
            if collisions_plot is not None:
                handles.append(collisions_plot)
            if changed_to_left_lane_plot is not None:
                handles.append(changed_to_left_lane_plot)
            if changed_to_right_lane_plot is not None:
                handles.append(changed_to_right_lane_plot)

            if (session.draw_mode & PLOT_LEGEND) != 0:
                plt.legend(handles=handles, loc="upper left", fontsize=10)
            else:
                plt.legend(handles=(), framealpha=0)

        return figure_title

    @staticmethod
    def __draw_events(session):
        changed_to_left_lane_plot = None
        changed_to_right_lane_plot = None
        collisions_plot = None
        for lane_change in session.lane_changes:
            if "Left" in lane_change[1]:
                changed_to_left_lane_plot, = plt.plot((lane_change[0], lane_change[0]), plt.ylim(), color=DARK_ORANGE,
                                                      label="Changed to Left Lane", zorder=-1)
            else:
                changed_to_right_lane_plot, = plt.plot((lane_change[0], lane_change[0]), plt.ylim(), color=ORANGE,
                                                       label="Changed to Right Lane", zorder=-1)

        for collision in session.collision_detections:
            collisions_plot, = plt.plot((collision[0], collision[0]), plt.ylim(), 'r', label="Collisions", zorder=-1)

        return (changed_to_left_lane_plot, changed_to_right_lane_plot, collisions_plot), None, "Events", "Events"

    @staticmethod
    def __draw_steering_wheel_movement(session):
        """
        :type session: SIMONDataPlotRequest
        :return: A tuple with the plot handles, y coordinate label, plot title and figure title
        :rtype: (tuple, str, str, str)
        """
        swm_mean_plot, = plt.plot(session.timestamps, session.steering_wheel_movement_means,
                                  'c', label="Mean")
        swm_sd_plot, = plt.plot(session.timestamps, session.steering_wheel_movement_standard_deviation,
                                'm', label="Standard Deviation")

        return (swm_mean_plot, swm_sd_plot), "Degrees", "Steering Wheel Movement", "Steering Wheel Movement"

    @staticmethod
    def __draw_pedals_movement(session):
        """
        :type session: SIMONDataPlotRequest
        :return: A tuple with the plot handles, y coordinate label, plot title and figure title
        :rtype: (tuple, str, str, str)
        """
        pm_throttle_plot, = plt.plot(session.timestamps, session.throttle_movement,
                                     'c', label="Throttle")
        pm_brake_plot, = plt.plot(session.timestamps, session.brake_movement,
                                  'b', label="Brake")

        return (pm_throttle_plot, pm_brake_plot), None, "Throttle and Brake Movement", "Throttle and Brake Movement"

    @staticmethod
    def __draw_lane_position_deviation(session):
        """
        :type session: SIMONDataPlotRequest
        :return: A tuple with the plot handles, y coordinate label, plot title and figure title
        :rtype: (tuple, str, str, str)
        """
        lpd_mean_plot, = plt.plot(session.timestamps, session.lane_position_deviation_mean,
                                  'c', label="Mean")
        lpd_sd_plot, = plt.plot(session.timestamps, session.lane_position_deviation_standard_deviation,
                                'm', label="Standard Deviation")

        return (lpd_mean_plot, lpd_sd_plot), "Meters", "Lane Position Deviation", "Lane Position Deviation"

    @staticmethod
    def __draw_lane_angle_deviation(session):
        """
        :type session: SIMONDataPlotRequest
        :return: A tuple with the plot handles, y coordinate label, plot title and figure title
        :rtype: (tuple, str, str, str)
        """
        lad_mean_plot, = plt.plot(session.timestamps, session.lane_angle_deviation_mean,
                                  'c', label="Mean")
        lad_sd_plot, = plt.plot(session.timestamps, session.lane_angle_deviation_standard_deviation,
                                'm', label="Standard Deviation")

        return (lad_mean_plot, lad_sd_plot), "Degrees", "Lane Angle Deviation", "Lane Angle Deviation"

    @staticmethod
    def __draw_instantaneous_speed(session):
        """
        :type session: SIMONDataPlotRequest
        :return: A tuple with the plot handles, y coordinate label, plot title and figure title
        :rtype: (tuple, str, str, str)
        """
        is_mean_plot, = plt.plot(session.timestamps, session.instantaneous_speed_mean,
                                 'c', label="Mean")
        is_sd_plot, = plt.plot(session.timestamps, session.instantaneous_speed_standard_deviation,
                               'm', label="Standard Deviation")

        return (is_mean_plot, is_sd_plot), "km/h", "Speed", "Speed"

    @staticmethod
    def __draw_heart_rate(session):
        """
        :type session: SIMONDataPlotRequest
        :return: A tuple with the plot handles, y coordinate label, plot title and figure title
        :rtype: (tuple, str, str, str)
        """
        hr_plot, = plt.plot(session.timestamps, session.heart_rate,
                            'c', label="Heart Rate")
        return tuple([hr_plot]), "BPM", "Heart Rate (Average of {:.1f} BPM)".format(session.heart_rate_mean), \
               "Heart Rate"

    # endregion

    # region Draw Mode Methods
    @classmethod
    def __get_draw_mode_print_string(cls, draw_mode):
        """
        :type draw_mode: int
        :rtype: str
        """
        string = ""
        if cls.__has_draw_mode(draw_mode, COMBINE_EVENTS):
            string += " & Combined Events"
        if cls.__has_draw_mode(draw_mode, PLOT_LEGEND):
            string += " & Legend"

        if string == "" and cls.__has_draw_mode(draw_mode, CLEAN_PLOT):
            return "Clean"

        return "With " + string[3:]

    @classmethod
    def __get_draw_mode_file_extension(cls, draw_mode):
        """
        :type draw_mode: int
        :rtype: str
        """
        extension = ""
        if cls.__has_draw_mode(draw_mode, COMBINE_EVENTS):
            extension += ".cEvents"
        if cls.__has_draw_mode(draw_mode, PLOT_LEGEND):
            extension += ".wLegend"

        if extension == "" and cls.__has_draw_mode(draw_mode, CLEAN_PLOT):
            return ".clean"

        return extension

    @staticmethod
    def __has_draw_mode(current_draw_mode, desired_draw_mode):
        """
        :type current_draw_mode: int
        :type desired_draw_mode: int
        :rtype: bool
        """
        return (current_draw_mode & desired_draw_mode) == desired_draw_mode
    # endregion
