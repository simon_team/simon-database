import psycopg2 as pg2
import matplotlib.pyplot as plt
import numpy as np
import sys

from data_extractor import SIMONDataExtractor
from data_plotter import SIMONDataPlotter, CLEAN_PLOT, COMBINE_EVENTS, PLOT_LEGEND, BULK_MODE

# def str2bool(v):
#     return str(v).lower() in ("yes", "true", "t", "1")
#
#
# def plot_all_data(rows):
#     dpa = dict()
#     for row in rows:
#         if (dpa.has_key(row[2])):
#             address_data = dpa[row[2]]
#         else:
#             address_data = []
#             dpa[row[2]] = address_data
#         address_data.append(row[4])
#
#     plt.figure("Steering Wheel Movement")
#     swm_string = np.array(dpa['/SIMON/Simulator/SteeringWheelMovement'])
#     swm = swm_string.astype(np.float)
#     plt.plot(swm[::2])
#     plt.plot(swm[1::2])
#     plt.show(False)
#
#     plt.figure("Pedals Movement")
#     pm_string = np.array(dpa['/SIMON/Simulator/PedalsMovement'])
#     pm = pm_string.astype(np.float)
#     plt.plot(pm[::2])
#     plt.plot(pm[1::2])
#     plt.show(False)
#
#     plt.figure("Lane Position Deviation")
#     lpd_string = np.array(dpa['/SIMON/Simulator/LanePositionDeviation'])
#     lpd = lpd_string.astype(np.float)
#     plt.plot(lpd[::2])
#     plt.plot(lpd[1::2])
#     plt.show(False)
#
#     plt.figure("Lane Angle Deviation")
#     lad_string = np.array(dpa['/SIMON/Simulator/LaneAngleDeviation'])
#     lad = lad_string.astype(np.float)
#     plt.plot(lad[::2])
#     plt.plot(lad[1::2])
#     plt.show(False)
#
#
# def plot_all_sessions(rows):
#     print "Number of registered rows:", len(rows)
#
#     data = []
#     for row in rows:
#         try:
#             if row[3] in ["Int32", "Int64"]:
#                 dyn_data = int(row[4])
#             elif row[3] in ["Single", "Double"]:
#                 dyn_data = float(row[4])
#             elif row[3] == "String":
#                 dyn_data = row[4]
#             elif row[3] == "Char":
#                 dyn_data = row[4]
#             elif row[3] == "Boolean":
#                 dyn_data = str2bool(row[4])
#             elif row[3] == None:
#                 dyn_data = None
#             else:
#                 print "ERROR: Invalid Type ->", type(row[3])
#         except:
#             print "ERROR: Invalid Data ->", row[3]
#
#         data.append((row[2], row[1], dyn_data))
#
#     data.sort(key=lambda entry: entry[1])
#
#     sessions = []
#     session = None
#     for data_entry in data:
#         address = data_entry[0]
#         if session == None:
#             if "Starting" in address:
#                 session = [data_entry]
#         else:
#             session.append(data_entry)
#             if "Stopping" in address:
#                 sessions.append(session)
#                 session = None
#
#     n_usefull_rows = 0
#     for session in sessions:
#         n_usefull_rows += len(session)
#     print "Number of usefull rows:", n_usefull_rows
#
#     n_rejected_rows = len(rows) - n_usefull_rows
#     print "Number of rejected rows:", (n_rejected_rows), str(round(n_rejected_rows * 100.0 / len(rows), 3)) + "%"
#
#     session_counter = 1
#     for session in sessions:
#         title = "Session " + str(session_counter)
#         print "Rendering " + title + "...",
#
#         fig_perf = plt.figure(title, figsize=(16, 9))
#
#         swm = np.array([entry[2] for entry in session if "SteeringWheelMovement" in entry[0]]).astype(np.float)
#         swm_mean = swm[::2]
#         swm_sd = swm[1::2]
#         plt.subplot(2, 2, 1)
#         plt.title("Steering Wheel Movement")
#         swm_mean_plot, = plt.plot(swm_mean, 'c', label="Mean")
#         swm_sd_plot, = plt.plot(swm_sd, 'm', label="Standard Deviation")
#         plt.xlim(0, len(swm_mean))
#         plt.ylim(-20, 15)
#         plt.legend(handles=[swm_mean_plot, swm_sd_plot], loc="upper left")
#
#         pm = np.array([entry[2] for entry in session if "PedalsMovement" in entry[0]]).astype(np.float)
#         pm_throttle = pm[::2]
#         pm_brake = pm[1::2]
#         plt.subplot(2, 2, 2)
#         plt.title("Throttle and Brake Movement Derivative")
#         pm_throttle_plot, = plt.plot(pm_throttle, 'c', label="Throttle")
#         pm_brake_plot, = plt.plot(pm_brake, 'b', label="Brake")
#         plt.xlim(0, len(pm_throttle))
#         plt.ylim(-.2, .25)
#         plt.legend(handles=[pm_throttle_plot, pm_brake_plot], loc="upper left")
#
#         lpd = np.array([entry[2] for entry in session if "LanePositionDeviation" in entry[0]]).astype(np.float)
#         lpd_mean = lpd[::2]
#         lpd_sd = lpd[1::2]
#         plt.subplot(2, 2, 3)
#         plt.title("Lane Position Deviation")
#         lpd_mean_plot, = plt.plot(lpd_mean, 'c', label="Mean")
#         lpd_sd_plot, = plt.plot(lpd_sd, 'm', label="Standard Deviation")
#         plt.xlim(0, len(lpd_mean))
#         plt.ylim(0, 25)
#         plt.legend(handles=[lpd_mean_plot, lpd_sd_plot], loc="upper left")
#
#         lad = np.array([entry[2] for entry in session if "LaneAngleDeviation" in entry[0]]).astype(np.float)
#         lad_mean = lad[::2]
#         lad_sd = lad[1::2]
#         plt.subplot(2, 2, 4)
#         plt.title("Lane Angle Deviation")
#         lad_mean_plot, = plt.plot(lad_mean, 'c', label="Mean")
#         lad_sd_plot, = plt.plot(lad_sd, 'm', label="Standard Deviation")
#         plt.xlim(0, len(lad_mean))
#         plt.ylim(-20, 15)
#         plt.legend(handles=[lad_mean_plot, lad_sd_plot], loc="upper left")
#
#         plt.subplots_adjust(.03, .03, .97, .97, .125, .15)
#         fig_perf.savefig("./" + title + ".png", dpi=300)
#
#         title += " (Heart Rate)"
#         fig_hr = plt.figure(title, figsize=(16, 4.5))
#         hr = np.array([entry[2] for entry in session if "HeartRateUpdate" in entry[0]]).astype(np.float)
#         hr_mean = hr.mean()
#         plt.title("Heart Rate")
#         hr_plot, = plt.plot(hr, 'c', linewidth=2.0, label="Raw Data")
#         hr_mean_plot, = plt.plot([0, len(hr)], [hr_mean, hr_mean], 'm', linewidth=2.0, label="Mean")
#         plt.xlim(0, len(hr))
#         plt.ylim(0, 110)
#         plt.legend(handles=[hr_plot, hr_mean_plot], loc="upper left")
#
#         plt.subplots_adjust(.03, .06, .97, .94, .125, .15)
#         fig_hr.savefig("./" + title + ".png", dpi=300)
#
#         plt.show(False)
#         print "Done!"
#         session_counter += 1


# try:
#     conn = pg2.connect("dbname='simon' user='postgres' host='localhost' password='root'")
# except:
#     print "Unable to connect to the database"
#     sys.exit(-1)
#
# cur = conn.cursor()
# cur.execute(
#     "SELECT p.id, p.timestamp, p.address, pd.data_type, pd.data FROM publications p LEFT OUTER JOIN publication_data pd ON (pd.publication_id = p.id)")
# rows = cur.fetchall()
#
# print len(rows)

# plot_all_sessions(rows)

if __name__ == "__main__":
    sde = SIMONDataExtractor()
    sde.connect_param('localhost', 'simon', 'postgres', 'root')
    sde.read_all_data()
    sessions = sde.get_all_sessions()
    sde.disconnect()

    sdp = SIMONDataPlotter()
    sdp.render_sessions(sessions, ".", draw_mode=PLOT_LEGEND | BULK_MODE)

    # raw_input("Press any key to close...")
