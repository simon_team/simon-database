import psycopg2 as pg2


class SIMONDataExtractor(object):
    __DATA_QUERY = "SELECT p.id, p.timestamp, p.address, pd.data_type, pd.data " \
                   "FROM publications p " \
                   "LEFT OUTER JOIN publication_data pd " \
                   "ON pd.publication_id = p.id"

    __STARTING_SIMULATION_ADDRESS = "Starting"
    __STOPPING_SIMULATION_ADDRESS = "Stopping"

    def __init__(self):
        self.__connection = None
        self.__rows = []
        self.__sessions = []

    def connect_param(self, host, db_name, user, password):
        """
        :type host: str
        :type db_name: str
        :type user: str
        :type password: str
        """
        self.connect("host='{}' dbname='{}' user='{}' password='{}'".format(host, db_name, user, password))

    def connect(self, connection_string):
        """
        :type connection_string: str
        """
        self.__connection = pg2.connect(connection_string)

    def disconnect(self):
        self.__connection.close()

    def read_all_data(self):
        cur = self.__connection.cursor()
        cur.execute(self.__DATA_QUERY)

        self.__rows = cur.fetchall()

        converted_data = self.__convert_data_by_type(self.__rows)
        self.__sessions = self.__divide_data_per_session(converted_data)

        self.__print_data_stats()

    def __print_data_stats(self):
        n_rows = len(self.__rows)
        n_useful_rows = sum([len(session) for session in self.__sessions])
        print("{} sessions in {} registered rows ({:.3f}% of them are useful)".format(
            len(self.__sessions), n_rows, n_useful_rows / float(n_rows) * 100))

    def get_session(self, session_idx):
        """
        :type session_idx: int
        :rtype: list
        """
        return self.__sessions[session_idx]

    def get_all_sessions(self):
        """
        :rtype: list
        """
        return self.__sessions

    @classmethod
    def __divide_data_per_session(cls, data):
        """
        :type data: list
        :rtype: list
        """
        sessions = []
        session = None
        for data_entry in data:
            address = data_entry[0]
            if session is None:
                if cls.__STARTING_SIMULATION_ADDRESS in address:
                    session = [data_entry]
            else:
                session.append(data_entry)
                if cls.__STOPPING_SIMULATION_ADDRESS in address:
                    sessions.append(session)
                    session = None
        return sessions

    @staticmethod
    def __convert_data_by_type(rows):
        """
        :type rows: list
        :rtype: list
        """
        data = []
        for row in rows:
            try:
                if row[3] in ["Int32", "Int64"]:
                    dyn_data = int(row[4])
                elif row[3] in ["Single", "Double"]:
                    dyn_data = float(row[4])
                elif row[3] == "String":
                    dyn_data = row[4]
                elif row[3] == "Char":
                    dyn_data = row[4]
                elif row[3] == "Boolean":
                    dyn_data = SIMONDataExtractor.__string_to_bool(row[4])
                elif row[3] is None:
                    dyn_data = None
                else:
                    print("ERROR: Invalid Type ->", type(row[3]))
                    continue
            except (ValueError, TypeError):
                print("ERROR: Invalid Data ->", row[3])
                continue

            data.append((row[2], row[1], dyn_data))

        data.sort(key=lambda entry: entry[1])
        return data

    @staticmethod
    def __string_to_bool(bool_string):
        """
        :type bool_string: str
        :rtype: bool
        """
        return str(bool_string).lower() in ("yes", "true", "t", "1")
