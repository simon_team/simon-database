CREATE TABLE publications (
    id serial NOT NULL,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    address text NOT NULL,
    CONSTRAINT publications_pkey PRIMARY KEY (id)
);

CREATE TABLE publication_data (
    id serial NOT NULL,
    publication_id integer NOT NULL,
    data_index integer NOT NULL,
    data_type text NOT NULL,
    data text,
    CONSTRAINT publication_data_pkey PRIMARY KEY (id),
    CONSTRAINT publications_fkey FOREIGN KEY (publication_id)
        REFERENCES public.publications (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);