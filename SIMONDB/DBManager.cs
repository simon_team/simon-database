﻿using Npgsql;
using System;
using System.Configuration;
using System.Net;
using System.Net.Sockets;

namespace SIMONDB
{
    class DBManager
    {
        const string CONNECTION_STRING_NAME = "DefaultDB";
        const string SELECT_PUBLICATION_BY_ID = "SELECT p.id, p.timestamp, p.address, pd.data_type, pd.data " +
            "FROM publications p LEFT OUTER JOIN publication_data pd ON (pd.publication_id = p.id) WHERE p.id = @pid";

        const string INSERT_PUBLICATION = "INSERT INTO publications (timestamp, address) VALUES (@t, @a) RETURNING id";

        const string INSERT_PUBLICATION_DATA = "INSERT INTO publication_data (publication_id, data_index, data_type, data) " +
            "VALUES (@pid, @idx, @type, @data) RETURNING id";

        string connString;
        public NpgsqlConnection conn;

        public bool Connect()
        {
            if (connString == null)
                connString = ConfigurationManager.ConnectionStrings[CONNECTION_STRING_NAME].ConnectionString;

            conn = new NpgsqlConnection(connString);
            try
            {
                conn.Open();
            }
            catch (SocketException)
            {
                return false;
            }
            catch (PostgresException e)
            {
                Console.Error.WriteLine($"(PostgresException) {e.Message}");
                return false;
            }

            return conn.State == System.Data.ConnectionState.Open;
        }

        public bool Connect(IPEndPoint endpoint, string username, string password, string dbName)
        {
            connString = $"Host={endpoint.Address};Port={endpoint.Port};Database={dbName};Username={username};Password={password};";
            return Connect();
        }

        public void Disconnect()
        {
            conn.Close();
        }

        public Publication GetPublication(int publicationId)
        {
            Publication publication = null;

            using (var cmd = new NpgsqlCommand(SELECT_PUBLICATION_BY_ID, conn))
            {
                cmd.Parameters.AddWithValue("pid", publicationId);
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        if (publication == null)
                            publication = new Publication(reader.GetInt32(0), reader.GetDateTime(1), reader.GetString(2));


                        if (reader.GetValue(3) is DBNull == false)
                            for (int dataIdx = 3; dataIdx < reader.FieldCount; dataIdx += 2)
                                publication.Data.Add(ParseData(reader.GetString(dataIdx), reader.GetString(dataIdx + 1)));
                    }
                }
            }

            return publication;
        }

        private object ParseData(string type, string data)
        {
            switch (type)
            {
                case "Int32":
                    return int.Parse(data);

                case "Int64":
                    return long.Parse(data);

                case "Single":
                    return float.Parse(data);

                case "Double":
                    return double.Parse(data);

                case "String":
                    return data;

                case "Char":
                    return char.Parse(data);

                case "Boolean":
                    return bool.Parse(data);

                default:
                    throw new ArgumentException("Unsupported data type.");
            }
        }

        public void InsertPublication(Publication publication)
        {
            InsertPublicationHeader(publication);
            InsertPublicationData(publication);
        }

        private int InsertPublicationHeader(Publication publication)
        {
            using (var cmd = new NpgsqlCommand(INSERT_PUBLICATION, conn))
            {
                cmd.Parameters.AddWithValue("t", publication.Timestamp);
                cmd.Parameters.AddWithValue("a", publication.Address);
                using (var reader = cmd.ExecuteReader())
                {
                    reader.Read();
                    publication.Id = reader.GetInt32(0);
                }
            }

            return publication.Id;
        }

        private void InsertPublicationData(Publication publication)
        {
            using (var cmd = new NpgsqlCommand(INSERT_PUBLICATION_DATA, conn))
            {
                cmd.Parameters.AddWithValue("pid", publication.Id);
                for (int idx = 0; idx < publication.Data.Count; idx++)
                {
                    using (var cloneCmd = cmd.Clone())
                    {
                        cloneCmd.Parameters.AddWithValue("idx", idx);
                        cloneCmd.Parameters.AddWithValue("type", publication.Data[idx].GetType().Name);
                        cloneCmd.Parameters.AddWithValue("data", publication.Data[idx]);
                        cloneCmd.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
