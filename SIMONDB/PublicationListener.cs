﻿using OSCD.Client;
using System;
using System.Collections.Generic;
using System.Net;
using System.Linq;
using System.Threading;

namespace SIMONDB
{
    class PublicationListener
    {
        OscdClient oscdClient;
        public event Action<string> OnSucess;
        public event Action<string> OnError;
        public event Action<Publication> OnPublication;

        Dictionary<string, MessageDataHandler> subscribedSubjects;

        Thread poolingThread;
        ManualResetEvent threadSignal;
        float threadSleepTime = 10;
        public float PoolingInterval
        {
            get
            {
                return threadSleepTime;
            }

            set
            {
                if (value <= 0)
                    throw new ArgumentOutOfRangeException("Value must be positive");
                else
                    threadSleepTime = value;
            }
        }

        public PublicationListener(IPEndPoint endpoint)
        {
            oscdClient = new OscdClient(endpoint);
            oscdClient.OnDisconnected += OnDisconnectedReceived;
            oscdClient.OnSubjectCancellationReceived += OnSubjectCancellationReceived;

            subscribedSubjects = new Dictionary<string, MessageDataHandler>();
        }

        public bool Connect()
        {
            if (!oscdClient.Connect())
                return false;
            else
                return oscdClient.RegisterUser(OnRegisterUserResponse);
        }

        public bool Disconnect()
        {
            CleanUp();
            return oscdClient.Disconnect();
        }

        private void OnDisconnectedReceived(object sender, EventArgs e)
        {
            CleanUp();
            OnError?.Invoke("DISCONNECTED");
        }

        private void StartPoolingThread()
        {
            threadSignal = new ManualResetEvent(false);
            poolingThread = new Thread(() =>
            {
                do
                {
                    GetAvailableSubjects();
                } while (!threadSignal.WaitOne((int)(threadSleepTime * 1000)));
            });

            poolingThread.Start();
        }

        private void CleanUp()
        {
            subscribedSubjects.Clear();
            threadSignal?.Set();
        }

        private void OnSubjectCancellationReceived(object sender, SubjectAlertReceivedEventArgs e)
        {
            subscribedSubjects.Remove(e.Subject);
            OnError?.Invoke($"{e.Subject} has been cancelled.");
        }

        private void OnRegisterUserResponse(object[] data)
        {
            bool? result;
            if (data.Length != 1 || (result = data[0] as bool?) != true)
                OnError?.Invoke("Invalid message received.");
            else
            {
                StartPoolingThread();
            }
        }

        private void GetAvailableSubjects()
        {
            if (oscdClient.GetAvailableSubjects(OnGetAvailableSubjectsResponse))
                OnSucess?.Invoke("Requested list of available subjects.");
            else
                OnError?.Invoke("Unable to request the available subjects.");
        }

        private void OnGetAvailableSubjectsResponse(object[] data)
        {
            if (data.Length != 2 || (data[0] as bool?) != true)
                OnSucess?.Invoke("No subjects available.");
            else
            {
                string[] subjects = (data[1] as string[]).Except(subscribedSubjects.Keys).Cast<string>().ToArray();
                if (subjects.Length == 0)
                {
                    OnSucess?.Invoke("No new subjects available.");
                    return;
                }

                OscdSubscription[] subscriptions = new OscdSubscription[subjects.Count()];
                for (int idx = 0; idx < subscriptions.Length; idx++)
                {
                    var subject = subjects[idx];
                    subscriptions[idx] = new OscdSubscription(
                        subject, (receivedData) =>
                        {
                            OnPublication?.Invoke(new Publication(DateTime.Now, subject, receivedData));
                        }, false);
                }

                if (oscdClient.SubscribeSubject(OnSubscribeSubject, subscriptions))
                {
                    foreach (var item in subscriptions)
                        subscribedSubjects.Add(item.Subject, item.Handler);

                    OnSucess?.Invoke("Requested subscription to the new subjects.");
                }
                else
                    OnError?.Invoke("Unable to request subscription to the new subjects.");
            }
        }

        private void OnSubscribeSubject(object[] data)
        {
            if (data.Length != 2 || (data[0] as bool?) != true || !(data[1] is KeyValuePair<string, bool>[]))
                OnError?.Invoke("Invalid message received.");
            else
                foreach (var item in data[1] as KeyValuePair<string, bool>[])
                    if (!item.Value)
                    {
                        subscribedSubjects.Remove(item.Key);
                        OnError?.Invoke($"Subscription to {item.Key} has not been successful.");
                    }
                    else
                        OnSucess?.Invoke($"Subscription to {item.Key} has been successful.");
        }
    }
}
