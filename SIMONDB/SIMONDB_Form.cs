﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SIMONDB
{
    public partial class SIMONDB_Form : Form
    {
        public class ConsoleLogWriter : TextWriter
        {
            public override Encoding Encoding
            {
                get { return Encoding.Default; }
            }

            private SIMONDB_Form form;
            private LogType logType;

            internal ConsoleLogWriter(SIMONDB_Form form, LogType logType)
            {
                this.form = form;
                this.logType = logType;
            }

            public override void Write(string value)
            {
                WriteLine(value);
            }

            public override void WriteLine(string value)
            {
                form.Invoke(form.logDelegate, value, logType);
            }
        }

        internal enum LogType { INFO, PUBLICATION, ERROR }
        internal delegate void LogDelegate(object log, LogType logType);
        internal LogDelegate logDelegate;
        private delegate void ErrorMessageBoxDelegate(string message, string caption);
        private delegate void ChangeControlTextDelegate(Control control, string text);

        private int nPublicationsReceived = 0;

        readonly Color InfoColor = Color.Blue;
        readonly Color ErrorColor = Color.Red;
        readonly Color PublicationColor = Color.DarkGreen;

        readonly string defaultDBConnectionStringName = "DefaultDB";
        readonly string defaultOSCDConnectionStringName = "DefaultOSCD";
        readonly string initFilePath = Path.Combine(Path.GetTempPath(), "simondb.ini");
        readonly string poolingIntervalValueLabelTemplate = "{0:00.0} seconds";

        DBManager dbManager;
        PublicationListener publicationListener;

        public SIMONDB_Form()
        {
            logDelegate = new LogDelegate(Log);

            Console.SetOut(new ConsoleLogWriter(this, LogType.INFO));
            Console.SetError(new ConsoleLogWriter(this, LogType.ERROR));

            InitializeComponent();

            poolingIntervalTrackBar_ValueChanged(this, null);
        }

        private void OnLoad(object sender, EventArgs e)
        {
            if (!PopulateControlPanelFromFile() && !PopulateControlPanelFromConfig())
                MessageBox.Show("Unable to load the default settings!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private Dictionary<string, string> LoadDbConnectionParameters()
        {
            var defaultDBConnectionString = ConfigurationManager.ConnectionStrings[defaultDBConnectionStringName]?.
                ConnectionString.Split(';', '=');
            if (defaultDBConnectionString == null)
                return null;

            Dictionary<string, string> dbConnectionParameters = new Dictionary<string, string>();
            for (int idx = 0; idx < defaultDBConnectionString.Length - 1; idx += 2)
                dbConnectionParameters.Add(defaultDBConnectionString[idx].ToLower(), defaultDBConnectionString[idx + 1]);

            return dbConnectionParameters;
        }

        private Dictionary<string, string> LoadOscdConnectionParameters()
        {
            var defaultOSCDConnectionString = ConfigurationManager.ConnectionStrings[defaultOSCDConnectionStringName]?.
                ConnectionString.Split(';', '=');
            if (defaultOSCDConnectionString == null)
                return null;

            Dictionary<string, string> oscdConnectionParameters = new Dictionary<string, string>();
            for (int idx = 0; idx < defaultOSCDConnectionString.Length - 1; idx += 2)
                oscdConnectionParameters.Add(defaultOSCDConnectionString[idx].ToLower(), defaultOSCDConnectionString[idx + 1]);

            return oscdConnectionParameters;
        }

        private bool PopulateControlPanelFromConfig()
        {
            var dbConnectionParameters = LoadDbConnectionParameters();
            if (dbConnectionParameters == null)
                return false;

            var oscdConnectionParameters = LoadOscdConnectionParameters();
            if (oscdConnectionParameters == null)
                return false;

            Dictionary<string, Dictionary<string, string>> connectionsParameters = new Dictionary<string, Dictionary<string, string>>();
            connectionsParameters.Add("DB", dbConnectionParameters);
            connectionsParameters.Add("OSCD", oscdConnectionParameters);

            return PopulateControlPanel(connectionsParameters);
        }

        private bool PopulateControlPanelFromFile()
        {
            if (!File.Exists(initFilePath))
                return false;

            Dictionary<string, Dictionary<string, string>> connectionsParameters;

            var formatter = new BinaryFormatter();
            var fileInfo = new FileInfo(initFilePath);
            using (var file = fileInfo.OpenRead())
                try
                {
                    connectionsParameters = (Dictionary<string, Dictionary<string, string>>)formatter.Deserialize(file);
                }
                catch (InvalidCastException)
                {
                    return false;
                }

            return PopulateControlPanel(connectionsParameters);
        }

        private bool PopulateDbControlPanel(Dictionary<string, string> dbConnectionParameters)
        {
            if (!dbConnectionParameters.TryGetValue("host", out string dbAddress))
                return false;

            if (!dbConnectionParameters.TryGetValue("port", out string dbPort))
                return false;

            if (!dbConnectionParameters.TryGetValue("database", out string dbName))
                return false;

            if (!dbConnectionParameters.TryGetValue("username", out string dbUsername))
                return false;

            if (!dbConnectionParameters.TryGetValue("password", out string dbPassword))
                return false;

            dbEndPointTextBox.Text = $"{(dbAddress)}:{dbPort}";
            dbNameTextBox.Text = dbName;
            dbUsernameTextBox.Text = dbUsername;
            dbPasswordTextBox.Text = dbPassword;

            return true;
        }

        private bool PopulateOscdControlPanel(Dictionary<string, string> oscdConnectionParameters)
        {
            if (!oscdConnectionParameters.TryGetValue("host", out string oscdAddress))
                return false;

            if (!oscdConnectionParameters.TryGetValue("port", out string oscdPort))
                return false;


            oscdEndPointTextBox.Text = $"{(oscdAddress)}:{oscdPort}";

            return true;
        }

        private bool PopulateControlPanel(Dictionary<string, Dictionary<string, string>> connectionsParameters)
        {
            if (!connectionsParameters.TryGetValue("DB", out Dictionary<string, string> dbConnectionParameters))
                return false;

            if (!connectionsParameters.TryGetValue("OSCD", out Dictionary<string, string> oscdConnectionParameters))
                return false;

            return PopulateDbControlPanel(dbConnectionParameters) && PopulateOscdControlPanel(oscdConnectionParameters);
        }

        private Dictionary<string, Dictionary<string, string>> GetConnectionParametersFromControlPanel()
        {
            Dictionary<string, Dictionary<string, string>> connectionsParameters = new Dictionary<string, Dictionary<string, string>>();

            Dictionary<string, string> dbConnectionParameters = new Dictionary<string, string>();
            connectionsParameters.Add("DB", dbConnectionParameters);
            var dbEndPoint = dbEndPointTextBox.Text.Split(':');

            Dictionary<string, string> oscdConnectionParameters = new Dictionary<string, string>();
            connectionsParameters.Add("OSCD", oscdConnectionParameters);
            var oscdEndPoint = oscdEndPointTextBox.Text.Split(':');

            dbConnectionParameters.Add("host", dbEndPoint[0]);
            dbConnectionParameters.Add("port", dbEndPoint[1]);
            dbConnectionParameters.Add("database", dbNameTextBox.Text);
            dbConnectionParameters.Add("username", dbUsernameTextBox.Text);
            dbConnectionParameters.Add("password", dbPasswordTextBox.Text);

            oscdConnectionParameters.Add("host", oscdEndPoint[0]);
            oscdConnectionParameters.Add("port", oscdEndPoint[1]);

            return connectionsParameters;
        }

        private void WriteInitFile()
        {
            var connectionParameters = GetConnectionParametersFromControlPanel();

            var formatter = new BinaryFormatter();
            var fileInfo = new FileInfo(initFilePath);
            using (var file = fileInfo.Create())
            {
                formatter.Serialize(file, connectionParameters);
                file.Flush();
            }
        }

        private void connectionButton_Click(object sender, EventArgs e)
        {
            switch (connectionButton.Text)
            {
                case "Connect":
                    connectButton_Click();
                    break;

                case "Disconnect":
                    Disconnect();
                    break;

                default:
                    throw new NotImplementedException();
            }
        }

        private void connectButton_Click()
        {
            connectionButton.Text = "Connecting";
            connectionButton.Enabled = false;
            if (ConnectDBManager())
            {
                Log("Connection Established!");
                connectionButton.Text = "Disconnect";
                WriteInitFile();
                MessageBox.Show("Connection Established!", "Connection Established", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                connectionButton.Text = "Connect";
            }
            connectionButton.Enabled = true;
        }

        private IPEndPoint ParseIPEndPoint(string[] endpoint)
        {
            if (endpoint.Length != 2)
                return null;
            else return ParseIPEndPoint(endpoint[0], endpoint[1]);
        }

        private IPEndPoint ParseIPEndPoint(string address, string port)
        {
            if (!IPAddress.TryParse(address, out IPAddress parsedAddress))
                if (address.ToLower() == "localhost")
                    parsedAddress = IPAddress.Loopback;
                else
                    return null;

            if (!ushort.TryParse(port, out ushort parsedPort))
                return null;

            return new IPEndPoint(parsedAddress, parsedPort);
        }

        private bool ConnectDBManager()
        {
            IPEndPoint endpoint = ParseIPEndPoint(dbEndPointTextBox.Text.Split(':'));
            if (endpoint == null)
            {
                MessageBox.Show("The database endpoint is not valid.", "Invalid Endpoint", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log("The database endpoint is not valid.", LogType.ERROR);
                return false;
            }
            else
            {
                dbManager = new DBManager();
                if (dbManager.Connect(endpoint, dbUsernameTextBox.Text, dbPasswordTextBox.Text, dbNameTextBox.Text))
                {
                    return ConnectPublicationListener();
                }
                else
                {
                    switch (MessageBox.Show("Unable to connect to the database.", "Database Connection",
                        MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1))
                    {
                        case DialogResult.Retry:
                            return ConnectDBManager();

                        case DialogResult.Cancel:
                            Disconnect();
                            Log("Unable to connect to the database.", LogType.ERROR);
                            return false;

                        default:
                            throw new NotImplementedException();
                    }
                }
            }
        }

        private bool ConnectPublicationListener()
        {
            IPEndPoint endpoint = ParseIPEndPoint(oscdEndPointTextBox.Text.Split(':'));
            if (endpoint == null)
            {
                MessageBox.Show("The OSCD endpoint is not valid.", "Invalid Endpoint", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Log("The OSCD endpoint is not valid.", LogType.ERROR);
                return false;
            }
            else
            {
                publicationListener = new PublicationListener(endpoint);
                publicationListener.PoolingInterval = poolingIntervalTrackBar.Value / 1000f;
                publicationListener.OnSucess += log => { Invoke(logDelegate, log, LogType.INFO); };
                publicationListener.OnError += OnListenerDisconnection;
                publicationListener.OnPublication += OnPublicationReceived;
                if (publicationListener.Connect())
                {
                    return true;
                }
                else
                {
                    switch (MessageBox.Show("Unable to connect to the OSCD.", "OSCD Connection",
                    MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1))
                    {
                        case DialogResult.Retry:
                            return ConnectPublicationListener();

                        case DialogResult.Cancel:
                            Disconnect();
                            Log("Unable to connect to the OSCD.", LogType.ERROR);
                            return false;

                        default:
                            throw new NotImplementedException();
                    }
                }
            }
        }

        private void OnListenerDisconnection(string log)
        {
            Invoke(logDelegate, log, LogType.ERROR);
            if (log == "DISCONNECTED")
            {
                Disconnect();
                Invoke(new ErrorMessageBoxDelegate((message, caption) =>
                {
                    MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }), log, "OSCD Disconnected");
            }
        }

        private void OnPublicationReceived(Publication publication)
        {
            dbManager.InsertPublication(publication);
            nPublicationsReceived++;
        }

        private void Disconnect()
        {
            dbManager?.Disconnect();
            publicationListener?.Disconnect();

            if (InvokeRequired)
                Invoke(new ChangeControlTextDelegate((control, text) =>
                {
                    control.Text = text;
                }), connectionButton, "Connect");
            else
                connectionButton.Text = "Connect";
        }

        private void Log(object log, LogType logType = LogType.INFO)
        {
            if (log == null)
                log = "NULL";
            else if (log as string == string.Empty)
                log = "EMPTY";

            int idx = logListBox.Items.Add($"{logType}: {log}");
            int visibleItems = logListBox.ClientSize.Height / logListBox.ItemHeight;
            logListBox.TopIndex = Math.Max(idx - visibleItems + 1, 0);

            nPublicationsReceivedLabel.Text = nPublicationsReceived.ToString();
        }

        private void logListBox_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();

            string text = logListBox.Items[e.Index].ToString();

            Color color;
            if (text.StartsWith("INFO"))
                color = InfoColor;
            else if (text.StartsWith("ERROR"))
                color = ErrorColor;
            else if (text.StartsWith("PUBLICATION"))
                color = PublicationColor;
            else
                color = e.ForeColor;

            e.Graphics.DrawString(text, e.Font, new SolidBrush(color), new PointF(e.Bounds.X, e.Bounds.Y));
        }

        private void logListBox_MeasureItem(object sender, MeasureItemEventArgs e)
        {
            var itemSize = TextRenderer.MeasureText(logListBox.Items[e.Index].ToString(), logListBox.Font);
            e.ItemHeight = itemSize.Height;
            if (logListBox.HorizontalExtent < itemSize.Width)
                logListBox.HorizontalExtent = itemSize.Width;
        }

        private void logListBox_DoubleClick(object sender, EventArgs e)
        {
            Clipboard.SetText(logListBox.SelectedItem.ToString());
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            logListBox.Items.Clear();
            nPublicationsReceived = 0;
        }

        private void poolingIntervalTrackBar_ValueChanged(object sender, EventArgs e)
        {
            var value = poolingIntervalTrackBar.Value / 1000f;
            poolingIntervalValueLabel.Text = string.Format(poolingIntervalValueLabelTemplate, value);

            if (publicationListener != null)
                publicationListener.PoolingInterval = value;
        }
    }
}