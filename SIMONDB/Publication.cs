﻿using System;
using System.Collections;

namespace SIMONDB
{
    public class Publication
    {
        public int Id { get; internal set; }
        public DateTime Timestamp { get; internal set; }
        public string Address { get; internal set; }
        public object[] DataArray { get { return Data.ToArray(); } }
        internal ArrayList Data;

        public Publication(DateTime timestamp, string address, object[] data) : this(-1, timestamp, address)
        {
            Data.AddRange(data);
        }
        public Publication(DateTime timestamp, string address) : this(-1, timestamp, address) { }

        internal Publication(int id, DateTime timestamp, string address)
        {
            Id = id;
            Timestamp = timestamp;
            Address = address;
            Data = new ArrayList();
        }

        public override string ToString()
        {
            string returnString = $"({Id} - {Timestamp}) {Address}: ";
            foreach (var item in Data)
                returnString += $"{item}, ";

            return returnString.Substring(0, returnString.Length - 2);
        }
    }
}