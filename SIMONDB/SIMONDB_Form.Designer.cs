﻿namespace SIMONDB
{
    partial class SIMONDB_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.controlPanel = new System.Windows.Forms.Panel();
            this.dbEndPointTextBox = new System.Windows.Forms.TextBox();
            this.oscdEndPointTextBox = new System.Windows.Forms.TextBox();
            this.dbNameTextBox = new System.Windows.Forms.TextBox();
            this.dbNameLabel = new System.Windows.Forms.Label();
            this.dbPasswordTextBox = new System.Windows.Forms.TextBox();
            this.dbPasswordLabel = new System.Windows.Forms.Label();
            this.dbUsernameTextBox = new System.Windows.Forms.TextBox();
            this.dbUsernameLabel = new System.Windows.Forms.Label();
            this.titleLabel = new System.Windows.Forms.Label();
            this.connectionButton = new System.Windows.Forms.Button();
            this.dbEndPointLabel = new System.Windows.Forms.Label();
            this.oscdEndPointLabel = new System.Windows.Forms.Label();
            this.dataPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.poolingIntervalValueLabel = new System.Windows.Forms.Label();
            this.poolingIntervalTrackBar = new System.Windows.Forms.TrackBar();
            this.clearButton = new System.Windows.Forms.Button();
            this.poolingIntervalTitleLabel = new System.Windows.Forms.Label();
            this.logListBox = new System.Windows.Forms.ListBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.nPublicationsReceivedLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.controlPanel.SuspendLayout();
            this.dataPanel.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.poolingIntervalTrackBar)).BeginInit();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // controlPanel
            // 
            this.controlPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.controlPanel.Controls.Add(this.dbEndPointTextBox);
            this.controlPanel.Controls.Add(this.oscdEndPointTextBox);
            this.controlPanel.Controls.Add(this.dbNameTextBox);
            this.controlPanel.Controls.Add(this.dbNameLabel);
            this.controlPanel.Controls.Add(this.dbPasswordTextBox);
            this.controlPanel.Controls.Add(this.dbPasswordLabel);
            this.controlPanel.Controls.Add(this.dbUsernameTextBox);
            this.controlPanel.Controls.Add(this.dbUsernameLabel);
            this.controlPanel.Controls.Add(this.titleLabel);
            this.controlPanel.Controls.Add(this.connectionButton);
            this.controlPanel.Controls.Add(this.dbEndPointLabel);
            this.controlPanel.Controls.Add(this.oscdEndPointLabel);
            this.controlPanel.Location = new System.Drawing.Point(13, 13);
            this.controlPanel.Name = "controlPanel";
            this.controlPanel.Size = new System.Drawing.Size(759, 58);
            this.controlPanel.TabIndex = 0;
            // 
            // dbEndPointTextBox
            // 
            this.dbEndPointTextBox.Location = new System.Drawing.Point(91, 34);
            this.dbEndPointTextBox.Name = "dbEndPointTextBox";
            this.dbEndPointTextBox.Size = new System.Drawing.Size(120, 20);
            this.dbEndPointTextBox.TabIndex = 13;
            // 
            // oscdEndPointTextBox
            // 
            this.oscdEndPointTextBox.Location = new System.Drawing.Point(91, 5);
            this.oscdEndPointTextBox.Name = "oscdEndPointTextBox";
            this.oscdEndPointTextBox.Size = new System.Drawing.Size(120, 20);
            this.oscdEndPointTextBox.TabIndex = 12;
            // 
            // dbNameTextBox
            // 
            this.dbNameTextBox.Location = new System.Drawing.Point(461, 5);
            this.dbNameTextBox.Name = "dbNameTextBox";
            this.dbNameTextBox.Size = new System.Drawing.Size(100, 20);
            this.dbNameTextBox.TabIndex = 11;
            // 
            // dbNameLabel
            // 
            this.dbNameLabel.AutoSize = true;
            this.dbNameLabel.Location = new System.Drawing.Point(402, 8);
            this.dbNameLabel.Name = "dbNameLabel";
            this.dbNameLabel.Size = new System.Drawing.Size(53, 13);
            this.dbNameLabel.TabIndex = 10;
            this.dbNameLabel.Text = "DB Name";
            // 
            // dbPasswordTextBox
            // 
            this.dbPasswordTextBox.Location = new System.Drawing.Point(296, 34);
            this.dbPasswordTextBox.Name = "dbPasswordTextBox";
            this.dbPasswordTextBox.PasswordChar = '●';
            this.dbPasswordTextBox.Size = new System.Drawing.Size(100, 20);
            this.dbPasswordTextBox.TabIndex = 9;
            // 
            // dbPasswordLabel
            // 
            this.dbPasswordLabel.AutoSize = true;
            this.dbPasswordLabel.Location = new System.Drawing.Point(217, 37);
            this.dbPasswordLabel.Name = "dbPasswordLabel";
            this.dbPasswordLabel.Size = new System.Drawing.Size(71, 13);
            this.dbPasswordLabel.TabIndex = 8;
            this.dbPasswordLabel.Text = "DB Password";
            // 
            // dbUsernameTextBox
            // 
            this.dbUsernameTextBox.Location = new System.Drawing.Point(296, 5);
            this.dbUsernameTextBox.Name = "dbUsernameTextBox";
            this.dbUsernameTextBox.Size = new System.Drawing.Size(100, 20);
            this.dbUsernameTextBox.TabIndex = 7;
            // 
            // dbUsernameLabel
            // 
            this.dbUsernameLabel.AutoSize = true;
            this.dbUsernameLabel.Location = new System.Drawing.Point(217, 8);
            this.dbUsernameLabel.Name = "dbUsernameLabel";
            this.dbUsernameLabel.Size = new System.Drawing.Size(73, 13);
            this.dbUsernameLabel.TabIndex = 6;
            this.dbUsernameLabel.Text = "DB Username";
            // 
            // titleLabel
            // 
            this.titleLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.titleLabel.Font = new System.Drawing.Font("nevis", 15F, System.Drawing.FontStyle.Bold);
            this.titleLabel.Location = new System.Drawing.Point(567, 3);
            this.titleLabel.Margin = new System.Windows.Forms.Padding(3);
            this.titleLabel.Name = "titleLabel";
            this.titleLabel.Size = new System.Drawing.Size(189, 52);
            this.titleLabel.TabIndex = 5;
            this.titleLabel.Text = "SIMON Database";
            this.titleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // connectionButton
            // 
            this.connectionButton.Location = new System.Drawing.Point(402, 32);
            this.connectionButton.Name = "connectionButton";
            this.connectionButton.Size = new System.Drawing.Size(159, 23);
            this.connectionButton.TabIndex = 4;
            this.connectionButton.Text = "Connect";
            this.connectionButton.UseVisualStyleBackColor = true;
            this.connectionButton.Click += new System.EventHandler(this.connectionButton_Click);
            // 
            // dbEndPointLabel
            // 
            this.dbEndPointLabel.AutoSize = true;
            this.dbEndPointLabel.Location = new System.Drawing.Point(3, 37);
            this.dbEndPointLabel.Name = "dbEndPointLabel";
            this.dbEndPointLabel.Size = new System.Drawing.Size(67, 13);
            this.dbEndPointLabel.TabIndex = 3;
            this.dbEndPointLabel.Text = "DB Endpoint";
            // 
            // oscdEndPointLabel
            // 
            this.oscdEndPointLabel.AutoSize = true;
            this.oscdEndPointLabel.Location = new System.Drawing.Point(3, 8);
            this.oscdEndPointLabel.Name = "oscdEndPointLabel";
            this.oscdEndPointLabel.Size = new System.Drawing.Size(82, 13);
            this.oscdEndPointLabel.TabIndex = 1;
            this.oscdEndPointLabel.Text = "OSCD Endpoint";
            // 
            // dataPanel
            // 
            this.dataPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dataPanel.Controls.Add(this.panel1);
            this.dataPanel.Controls.Add(this.logListBox);
            this.dataPanel.Location = new System.Drawing.Point(12, 77);
            this.dataPanel.Name = "dataPanel";
            this.dataPanel.Size = new System.Drawing.Size(760, 472);
            this.dataPanel.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.poolingIntervalValueLabel);
            this.panel1.Controls.Add(this.poolingIntervalTrackBar);
            this.panel1.Controls.Add(this.clearButton);
            this.panel1.Controls.Add(this.poolingIntervalTitleLabel);
            this.panel1.Location = new System.Drawing.Point(4, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(753, 29);
            this.panel1.TabIndex = 1;
            // 
            // poolingIntervalValueLabel
            // 
            this.poolingIntervalValueLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.poolingIntervalValueLabel.AutoSize = true;
            this.poolingIntervalValueLabel.Location = new System.Drawing.Point(546, 8);
            this.poolingIntervalValueLabel.Name = "poolingIntervalValueLabel";
            this.poolingIntervalValueLabel.Size = new System.Drawing.Size(74, 13);
            this.poolingIntervalValueLabel.TabIndex = 18;
            this.poolingIntervalValueLabel.Text = "XX.X seconds";
            // 
            // poolingIntervalTrackBar
            // 
            this.poolingIntervalTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.poolingIntervalTrackBar.AutoSize = false;
            this.poolingIntervalTrackBar.LargeChange = 500;
            this.poolingIntervalTrackBar.Location = new System.Drawing.Point(88, 6);
            this.poolingIntervalTrackBar.Maximum = 30000;
            this.poolingIntervalTrackBar.Minimum = 100;
            this.poolingIntervalTrackBar.Name = "poolingIntervalTrackBar";
            this.poolingIntervalTrackBar.Size = new System.Drawing.Size(452, 20);
            this.poolingIntervalTrackBar.SmallChange = 100;
            this.poolingIntervalTrackBar.TabIndex = 17;
            this.poolingIntervalTrackBar.TickFrequency = 1000;
            this.poolingIntervalTrackBar.Value = 5000;
            this.poolingIntervalTrackBar.ValueChanged += new System.EventHandler(this.poolingIntervalTrackBar_ValueChanged);
            // 
            // clearButton
            // 
            this.clearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clearButton.Location = new System.Drawing.Point(675, 3);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(75, 23);
            this.clearButton.TabIndex = 14;
            this.clearButton.Text = "Clear Log";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // poolingIntervalTitleLabel
            // 
            this.poolingIntervalTitleLabel.AutoSize = true;
            this.poolingIntervalTitleLabel.Location = new System.Drawing.Point(4, 8);
            this.poolingIntervalTitleLabel.Name = "poolingIntervalTitleLabel";
            this.poolingIntervalTitleLabel.Size = new System.Drawing.Size(80, 13);
            this.poolingIntervalTitleLabel.TabIndex = 15;
            this.poolingIntervalTitleLabel.Text = "Pooling Interval";
            // 
            // logListBox
            // 
            this.logListBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.logListBox.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawVariable;
            this.logListBox.FormattingEnabled = true;
            this.logListBox.HorizontalScrollbar = true;
            this.logListBox.IntegralHeight = false;
            this.logListBox.Location = new System.Drawing.Point(4, 38);
            this.logListBox.Name = "logListBox";
            this.logListBox.Size = new System.Drawing.Size(751, 429);
            this.logListBox.TabIndex = 0;
            this.logListBox.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.logListBox_DrawItem);
            this.logListBox.MeasureItem += new System.Windows.Forms.MeasureItemEventHandler(this.logListBox_MeasureItem);
            this.logListBox.DoubleClick += new System.EventHandler(this.logListBox_DoubleClick);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nPublicationsReceivedLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 539);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(784, 22);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "statusStrip1";
            // 
            // nPublicationsReceivedLabel
            // 
            this.nPublicationsReceivedLabel.Name = "nPublicationsReceivedLabel";
            this.nPublicationsReceivedLabel.Size = new System.Drawing.Size(13, 17);
            this.nPublicationsReceivedLabel.Text = "0";
            this.nPublicationsReceivedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SIMONDB_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(784, 561);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.dataPanel);
            this.Controls.Add(this.controlPanel);
            this.MinimumSize = new System.Drawing.Size(710, 250);
            this.Name = "SIMONDB_Form";
            this.Text = "SIMON Database";
            this.Load += new System.EventHandler(this.OnLoad);
            this.controlPanel.ResumeLayout(false);
            this.controlPanel.PerformLayout();
            this.dataPanel.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.poolingIntervalTrackBar)).EndInit();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel controlPanel;
        private System.Windows.Forms.Panel dataPanel;
        private System.Windows.Forms.Button connectionButton;
        private System.Windows.Forms.Label dbEndPointLabel;
        private System.Windows.Forms.Label oscdEndPointLabel;
        private System.Windows.Forms.Label titleLabel;
        private System.Windows.Forms.ListBox logListBox;
        private System.Windows.Forms.Label dbUsernameLabel;
        private System.Windows.Forms.TextBox dbUsernameTextBox;
        private System.Windows.Forms.TextBox dbPasswordTextBox;
        private System.Windows.Forms.Label dbPasswordLabel;
        private System.Windows.Forms.TextBox dbNameTextBox;
        private System.Windows.Forms.Label dbNameLabel;
        private System.Windows.Forms.TextBox dbEndPointTextBox;
        private System.Windows.Forms.TextBox oscdEndPointTextBox;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TrackBar poolingIntervalTrackBar;
        private System.Windows.Forms.Label poolingIntervalTitleLabel;
        private System.Windows.Forms.Label poolingIntervalValueLabel;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel nPublicationsReceivedLabel;
    }
}

